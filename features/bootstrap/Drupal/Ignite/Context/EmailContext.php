<?php

namespace Drupal\Ignite\Context;

use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Gherkin\Node\TableNode;
use Exception;

final class EmailContext extends RawMinkContext
{
    /**
     * @BeforeScenario @mail
     */
    public function setUpMailScenario()
    {
        // save the default mailing system
        $this->defaultMailSystem = variable_get('mail_system');

        // set the devel debug mailing system
        variable_set('mail_system', ['default-system' => 'DevelMailLog']);

        // set the email directory path
        $this->emailDirectoryPath = variable_get('file_temporary_path') . DIRECTORY_SEPARATOR . 'devel-mails';

        // removes the directory to get a clean slate
        if (file_exists($this->emailDirectoryPath)) {
            $this->rrmdir($this->emailDirectoryPath);
        }

        // creates the email directory
        if (!file_exists($this->emailDirectoryPath)) {
            mkdir($this->emailDirectoryPath);
        }
    }

    /**
     * @AfterScenario @mail
     */
    public function tearDownMailScenario()
    {
        // removes the directory
        if (file_exists($this->emailDirectoryPath)) {
            $this->rrmdir($this->emailDirectoryPath);
        }

        // reset the default mailing system
        variable_set('mail_system', $this->defaultMailSystem);
    }

    /**
     * @Given /^the email message with subject "([^"]*)" was sent to the following email addresses:$/
     */
    public function wasMessageSentToEmails($subject, TableNode $emails)
    {
        $sentEmails = $this->getSentEmails();

        foreach ($sentEmails as $sentEmail) {
            if (false === strstr($sentEmail, $subject)) {
                throw new Exception(sprintf(
                    'Subject "%s" not found in message "%s".',
                    $subject,
                    $sentEmail
                ));
            }

            $emailsNotFound = [];

            foreach ($emails as $email) {
                if (false === strstr($sentEmail, $email)) {
                    $emailsNotFound[] = $email;
                }
            }

            if (empty($emailsNotFound)) {
                continue;
            }

            throw new Exception(sprintf(
                'Email with subject "%s" has not been sent to the following addresses: "%s"',
                $subject,
                implode(', ', $emailsNotFound)
            ));
        }
    }

    /**
     * @Given /^(\d+) emails should have been sent$/
     */
    public function countSentEmails($emailCount)
    {
        $emails = $this->getSentEmails();

        if (count($emails) !== (int) $emailCount) {
            throw new Exception(sprintf(
                '%d emails have been sent instead of %d.',
                count($emails),
                (int) $emailCount
            ));
        }
    }

    /**
     * @Transform /^table:email$/
     */
    public function castEmailsTable(TableNode $emailsTable)
    {
        $emails = [];

        foreach ($emailsTable->getHash() as $emailHash) {
            $emails[] = $emailHash['email'];
        }

        return $emails;
    }

    /**
     * Return list of emails.
     *
     * @return array
     */
    private function getSentEmails()
    {
        $emails = scandir($this->emailDirectoryPath);

        $emails = array_filter($emails, function ($name) {
            return !($name === '.' || $name === '..');
        });

        return array_map(function ($email) {
            return file_get_contents($this->emailDirectoryPath . DIRECTORY_SEPARATOR . $email);
        }, $emails);
    }

    /**
     * Recursively removes a directory.
     *
     * @param string $dir
     */
    private function rrmdir($dir) {
      foreach (glob($dir . '/*') as $file) {
        if ($file === '.' || $file === '..') {
          continue;
        }

        if (is_dir($file)) {
          $this->rrmdir($file);
        } else {
          unlink($file);
        }
      }

      rmdir($dir);
    }
}
