core = 7.x

api = 2

projects[] = "drupal"

; Modules

;; Contrib

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.7"

projects[context][subdir] = "contrib"
projects[context][version] = "3.6"

projects[date][subdir] = "contrib"
projects[date][version] = "2.8"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.6"

projects[features][subdir] = "contrib"
projects[features][version] = "2.4"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.2"

projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.0-beta1"

projects[migrate][subdir] = "contrib"
projects[migrate][version] = "2.7"

projects[migrate_extras][subdir] = "contrib"
projects[migrate_extras][version] = "2.5"

projects[references][subdir] = "contrib"
projects[references][version] = "2.1"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.9"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[token][subdir] = "contrib"
projects[token][version] = "1.6"

projects[webform][subdir] = "contrib"
projects[webform][version] = "4.5"

projects[views][subdir] = "contrib"
projects[views][version] = "3.10"

;; Devel
projects[coder][subdir] = "devel"
projects[coder][version] = "2.2"

projects[devel][subdir] = "devel"
projects[devel][version] = "1.5"

projects[diff][subdir] = "devel"
projects[diff][version] = "3.2"

projects[masquerade][subdir] = "devel"
projects[masquerade][version] = "1.0-rc7"

; Themes
projects[tao][version] = "3.1"
projects[rubik][version] = "4.0"

; Libraries
