<?php
/**
 * @file
 * euci_blog.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function euci_blog_taxonomy_default_vocabularies() {
  return array(
    'topics' => array(
      'name' => 'topics',
      'machine_name' => 'topics',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
