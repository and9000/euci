<?php
/**
 * @file
 * euci_blog.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function euci_blog_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_blog-de:blog/95
  $menu_links['main-menu_blog-de:blog/95'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blog/95',
    'router_path' => 'blog',
    'link_title' => 'Blog DE',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_blog-de:blog/95',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_blog-fr:blog/94
  $menu_links['main-menu_blog-fr:blog/94'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blog/94',
    'router_path' => 'blog',
    'link_title' => 'Blog FR',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_blog-fr:blog/94',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_blog-it:blog/96
  $menu_links['main-menu_blog-it:blog/96'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blog/96',
    'router_path' => 'blog',
    'link_title' => 'Blog IT',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_blog-it:blog/96',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Blog DE');
  t('Blog FR');
  t('Blog IT');


  return $menu_links;
}
